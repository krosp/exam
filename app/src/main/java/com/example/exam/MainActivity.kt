package com.example.exam

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
/// основное окно
class MainActivity : AppCompatActivity() {

    //хранилище
    var mSettings: SharedPreferences? = null
///    интерфейс
    var etFilter:EditText? = null
    var btGetInfo:Button? = null
    var lvInfo:ListView? = null
    var btGetHistory:Button? = null
    var tvTest:TextView? = null

///функция при запуске
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//Хранилище, инициаолизация
        mSettings = getSharedPreferences("Data", Context.MODE_PRIVATE);

        etFilter = findViewById(R.id.et_filter)
        btGetInfo = findViewById(R.id.bt_get_info)
        lvInfo = findViewById(R.id.lv_info)
        btGetHistory = findViewById(R.id.bt_get_history)
        tvTest = findViewById(R.id.tv_test)

        val intentHistory = Intent(this, HistoryActivity::class.java)

            //обработка нажатия на кнопку получения данных
        btGetInfo?.setOnClickListener {
            val test = GetQuestions(this)
            test.execute(etFilter?.text.toString())
        }
            //обработка нажати на кнопку перехода в историю
        btGetHistory?.setOnClickListener{
            startActivity(intentHistory)
        }
    }
}

//класс хранения данных
class Color(  var color:String = "",
              var description:String = "",
              var longName:String = "",
              var id:String = "")
{
    // функция получения данных
    fun getMainInfo():String
    {
        return "\nЦвет: $id:$longName \nкод: $color \nописание: $description\n"
    }
}

class Parser()
{
    //функция получения массива (если нужны параметры-массивы тодо)
    fun getArray(jsonArray: JSONArray):MutableList<String> {

        val result:MutableList<String> = MutableList(0) {""}

        for(i in 0 until jsonArray.length())
            result.add(jsonArray.getString(i))

        return result;

    }

    //парсинг json
    fun getColorsInfo(JSON:String): MutableList<Color> {
        val colors = MutableList(0){Color()}

        if(JSON !== "") {
            val data = JSONObject(JSON)
            var colorsArray:JSONArray?=null
            try {
                colorsArray = data.getJSONArray("data")
            } catch (e:Exception) {

            }
            if(colorsArray==null)
            {
                val color = Color()
                val colorObject = data.getJSONObject("data")

                color.id = colorObject.getString("id")
                val atributes = colorObject.getJSONObject("attributes")
                color.longName = atributes.getString("long_name")
                color.description = atributes.getString("description")
                color.color = atributes.getString("color")

                colors.add(color)
            } else {
                for(i in 0 until colorsArray.length())
                {
                    val color = Color()
                    val colorObject = colorsArray.getJSONObject(i)

                    color.id = colorObject.getString("id")
                    val atributes = colorObject.getJSONObject("attributes")
                    color.longName = atributes.getString("long_name")
                    color.description = atributes.getString("description")
                    color.color = atributes.getString("color")

                    colors.add(color)
                }
            }
        }

        return colors
    }
}

class GetQuestions(val mainActivity: MainActivity) : AsyncTask<String, Void, String>() {

    var query = ""
        //получение json с сервера
    override fun doInBackground(vararg params: String?): String {
        query = params[0].toString()

        var data = "";
        val connection = URL("https://api-v3.mbta.com/routes/"+params[0]).openConnection() as HttpURLConnection
        try {
            data = connection.inputStream.bufferedReader().use { it.readText() }
        } catch (e:Exception) {

        } finally {
            connection.disconnect()
        }

        return data;
    }

    //парсинг и вывод в лист
    @SuppressLint("SetTextI18n")
    override fun onPostExecute(result: String) {
        super.onPostExecute(result)
        var queries = "";
        if(result!=="") {
            mainActivity.tvTest?.text = "ok"

            // todo получить прошлые запросы

            if (mainActivity.mSettings?.contains("queries")!!)
                queries = mainActivity.mSettings?.getString("queries", "").toString()

            // todo дополнить прошлые запросы
            if(query == "")
                query = "&"
            queries += " $query"
            val editor = mainActivity.mSettings?.edit()
            editor?.putString("queries", queries)
            editor?.putString(query, result)
            editor?.apply()

            val colors = Parser().getColorsInfo(result)
            val mainStr: MutableList<String> = MutableList(0) { "" }
            colors.forEach { mainStr += it.getMainInfo() }

            // todo Вставить в адаптер лист и вывести
            val adapter: ArrayAdapter<String> = ArrayAdapter(mainActivity, android.R.layout.simple_list_item_1, mainStr)
            mainActivity.lvInfo?.adapter = adapter
        }

        // todo Вывести статус результата запроса
        else
        {
            mainActivity.tvTest?.text = "error"
            mainActivity.lvInfo?.adapter = null
        }

    }
}