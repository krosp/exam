package com.example.exam

import android.content.Context
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity

class HistoryActivity : AppCompatActivity() {


    //Элементы интерфейса
    var btGoInBack: Button? = null
    var lwHistory: ListView? = null
    var queriesWords: List<String>? = null
    var isHistoryNames = true;
    var adapterNames: ArrayAdapter<String>? = null
    var adapterValues: ArrayAdapter<String>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        btGoInBack  = findViewById<Button>(R.id.bt_go_in_back)
        lwHistory  = findViewById<ListView>(R.id.lv_history)
        val mSettings = getSharedPreferences("Data", Context.MODE_PRIVATE);

        //Достаем данные из хранилища
        var queries = ""
        if (mSettings.contains("queries"))
        {
            queries = mSettings.getString("queries", "").toString()
            queries = queries.trim()
            queriesWords = queries.split(" ")

            adapterNames = ArrayAdapter(this, android.R.layout.simple_list_item_1, queriesWords!!)
            lwHistory?.adapter = adapterNames
        }

        //нажатие на кнопку возврата
        btGoInBack?.setOnClickListener {
            if(isHistoryNames)
            {
                this.finish()
            } else {
                lwHistory?.adapter = adapterNames
                isHistoryNames = true
            }
        }

        //Нажатие на элемент списка
        lwHistory?.setOnItemClickListener { parent, view, position, id ->
            if(isHistoryNames)
            {
                isHistoryNames = false
                var parameter = queriesWords?.get(id.toInt())
                if (mSettings.contains(parameter))
                {
                    if(parameter == "&")
                        parameter = ""
                    parameter = mSettings.getString(parameter, "").toString()
                    val colors = Parser().getColorsInfo(parameter)
                    val colorsStr = MutableList<String>(0){""}
                    colors.forEach { colorsStr.add(it.getMainInfo()) }

                    adapterValues = ArrayAdapter(this, android.R.layout.simple_list_item_1, colorsStr)
                    lwHistory?.adapter = adapterValues
                }
            }
        }
    }
}