package com.example.exam

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.exam", appContext.packageName)

        Espresso.onView(ViewMatchers.withId(R.id.et_filter)).perform(ViewActions.clearText())
        Espresso.onView(ViewMatchers.withId(R.id.et_filter)).perform(ViewActions.typeText("Orange"))
        Espresso.onView(ViewMatchers.withId(R.id.bt_get_info)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.tv_test)).check(
            ViewAssertions.matches(
                ViewMatchers.withText("ok")))

        Espresso.onView(ViewMatchers.withId(R.id.et_filter)).perform(ViewActions.clearText())
        Espresso.onView(ViewMatchers.withId(R.id.et_filter)).perform(ViewActions.typeText("123312"))
        Espresso.onView(ViewMatchers.withId(R.id.bt_get_info)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.tv_test)).check(
            ViewAssertions.matches(
                ViewMatchers.withText("error")))
    }
}