package com.example.exam

import org.json.JSONObject
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        //тест 0))))
        assertEquals(4, 2 + 2)

        var color = Parser().getColorsInfo("{\"data\":{\"attributes\":{\"color\":\"ED8B00\",\"description\":\"Rapid Transit\",\"direction_destinations\":[\"Forest Hills\",\"Oak Grove\"],\"direction_names\":[\"South\",\"North\"],\"fare_class\":\"Rapid Transit\",\"long_name\":\"Orange Line\",\"short_name\":\"\",\"sort_order\":10020,\"text_color\":\"FFFFFF\",\"type\":1},\"id\":\"Orange\",\"links\":{\"self\":\"/routes/Orange\"},\"relationships\":{\"line\":{\"data\":{\"id\":\"line-Orange\",\"type\":\"line\"}},\"route_patterns\":{}},\"type\":\"route\"},\"jsonapi\":{\"version\":\"1.0\"}}")
        var Orange = color.get(0).getMainInfo()
        var OrangeReal = "\n" +
                "Цвет: Orange:Orange Line \n" +
                "код: ED8B00 \n" +
                "описание: Rapid Transit\n"

        assertEquals(OrangeReal, Orange)

        color = Parser().getColorsInfo("{\"data\":{\"attributes\":{\"color\":\"DA291C\",\"description\":\"Rapid Transit\",\"direction_destinations\":[\"Ashmont/Braintree\",\"Alewife\"],\"direction_names\":[\"South\",\"North\"],\"fare_class\":\"Rapid Transit\",\"long_name\":\"Red Line\",\"short_name\":\"\",\"sort_order\":10010,\"text_color\":\"FFFFFF\",\"type\":1},\"id\":\"Red\",\"links\":{\"self\":\"/routes/Red\"},\"relationships\":{\"line\":{\"data\":{\"id\":\"line-Red\",\"type\":\"line\"}},\"route_patterns\":{}},\"type\":\"route\"},\"jsonapi\":{\"version\":\"1.0\"}}")
        Orange = color.get(0).getMainInfo()
        OrangeReal = "\n" +
                "Цвет: Red:Red Line \n" +
                "код: DA291C \n" +
                "описание: Rapid Transit\n"

        assertEquals(OrangeReal, Orange)
    }
}